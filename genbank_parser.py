#!/usr/bin/env python
import urllib.request

'''
Script that retrieves the GenBank evolution content by parsing its release note html content.
'''

__author__ = "Julien Mariethoz"
__version__ = "1.0"


def genebank_release_parser():
    csv_file = 'genbank_releases.csv'
    lines = []
    header = ['release','date_month','date_year','nb_loci','nb_bases','nb_sequences']

    lines.append((",".join(header) + '\n'))
    first_release = 74
    current_release = 240
    for i in range(first_release,current_release):
        line = release_data(i)
        lines.append(",".join(line) + '\n')

    csv_writer(csv_file, lines)


def release_data(rel_nb):
    url = 'https://www.ncbi.nlm.nih.gov/genbank/release/' + str(rel_nb) + '/GBREL.TXT/'
    response = urllib.request.urlopen(url)
    html = response.read().decode('utf-8')
    data = clean(html)
    date = date_extractor(data[0]) # line after 'Genetic Sequence Data Bank'
    numbers = loci_extractor(data[1]) # non empty line after 'Distribution Release Notes'
    print()
    result = []
    result.append(str(rel_nb))
    for date_item in date:
        result.append(date_item)
    for numbers_item in numbers:
        result.append(numbers_item)

    return result

def clean(html):
    result = []
    html = html.split('Genetic Sequence Data Bank')[1]
    html = html.split('This document describes')[0]
    html = html.replace('\n\n', '\n')
    for row in html.split('\n'):
        row = row.strip()
        if row != '':
            if not 'Release' in row:
                result.append(row)
    print(result)
    return result

def date_extractor(date_text):
    array = date_text.split(' ')

    month_map = {
        'January': 1,
        'February': 2,
        'March': 3,
        'April': 4,
        'May': 5,
        'June': 6,
        'July': 7,
        'August': 8,
        'September': 9,
        'October': 10,
        'November': 11,
        'December': 12,
    }

    date_array = []
    month = ''
    if array[0] in month_map.keys():
        month = month_map[array[0]]
    elif array[1] in month_map.keys():
        month = month_map[array[1]]

    date_array.append(str(month))
    date_array.append(array[2])

    #print(date_array)
    return date_array


def loci_extractor(count_text):
    result = []
    array = count_text.split(',')
    loci = ''
    bases = ''
    sequences = ''

    for item in array:
        if 'loci' in item :
            loci = item.replace('loci', '')
            loci = loci.strip()

        if 'bases' in item :
            bases = item.replace('bases', '')
            bases = bases.strip()

        if 'sequences' in item :
            sequences = item.replace('from', '')
            sequences = sequences.replace('reported', '')
            sequences = sequences.replace('sequences', '')
            sequences = sequences.strip()

    result.append(loci)
    result.append(bases)
    result.append(sequences)

    print(count_text)
    print(result)
    return result

def csv_writer(csv_file, lines):
    with open(csv_file, 'w') as outfile:
        outfile.writelines(lines)


if __name__ == '__main__':
    genebank_release_parser()
